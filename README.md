# Wing
#### Il servizio digitale che recupera pazienti e cittadini rimasti indietro nella prevenzione oncologica e nel loro follow up.

## 👉 [Link al prototipo interattivo](https://xd.adobe.com/view/959e0628-7959-42f0-9823-fe5aa5a67cad-c0cd/) 🖥
## 👉 [Link alla video presentazione](https://www.youtube.com/watch?v=JJQJQXfgvlE) 📹



## Promotional Image

![](promo.png)


##  Development Process

#### Il processo che abbiamo affrontato è stato condotto con successo tramite il metodo della CBL. La challenge da affrontare è stata investigata attraverso la raccolta di fonti e dati statistici relativi alla situazione causata dalla pandemia nel 2020. 
#### La natura iterativa del framework CBL ci ha consentito di replicare il processo investigativo a ogni potenziale blocco incontrato sia prima che dopo le sessioni di Checkpoint e Q&A senza perdere il lavoro precedente. Nella documentazione allegata in [BeyondCovid-19__Research.pdf](BeyondCovid-19__Research.pdf) e [Brainwriting.pdf](Brainwriting.pdf) è possibile visualizzare questo processo attraverso i nostri appunti. 
#### Alla fase investigativa è seguito uno studio della usability e della fattibilità. Esperienze pregresse con il SSN ci hanno permesso di capire i limiti del sistema (carico di lavoro, comunicazione, infrastrutture tecnologiche) e quindi di ridimensionare la solution per renderla applicabile in contesti reali. L'interfaccia è stata quindi studiata tenendo anche conto di tali limitazioni. 
#### Un secondo check da parte dei mentor MSD ci ha permesso di confermare la corretta direzione presa e perfezionare il prototipo.
